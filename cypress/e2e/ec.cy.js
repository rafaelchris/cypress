// describe a suite
describe('My First Test', () => {
  // define the test case
  it('Open EC and navigate to', () => {
    // visit data explorer page
    cy.visit('https://analysis-tools.app.ecocommons.org.au/')

    cy.get('a').contains('Modelling Wizards').click()

    cy.get('a').contains('Biodiversity and').click()

    cy.get('a').contains('Species Distribution Modelling Experiment').click()

  })
})