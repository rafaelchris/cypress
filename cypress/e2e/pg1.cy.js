// describe a suite
describe('My Demo Test', () => {
  // define the test case
  it('Demo Test', () => {
    // visit EcoCommons website
    cy.visit('https://testsheepnz.github.io/BasicCalculator.html')

    cy.get('#selectBuild').select('1')

    cy.get('[name="number1"]').type('5')

    cy.get('[name="number2"]').type('3')

    cy.get('#selectBuild').select('Prototype')

    cy.get('#selectOperationDropdown').select('Subtract')

    cy.get('#integerSelect').check()

    cy.get('#calculateButton').click()

    cy.get('#clearButton').click()
    

  })
})
