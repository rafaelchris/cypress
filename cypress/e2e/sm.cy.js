// describe a suite
describe('My First Test', () => {
  // define the test case
  it('Visits the Kitchen', () => {
    // visit EcoCommons website
    cy.visit('https://ecocommons.org.au')

    // click on 'Our Team' url
    cy.get('[id="menu-item-1168"]').contains('Our team').click({force:true});

    // assert: new page loaded
    cy.url().should('contains', '/about/team/')

    // type in search bar
    cy.get('input[type="search"]').type('NASA')

    // assert: value typed
    cy.get('input[type="search"]').should('have.value', 'NASA')

    // submit search
    cy.get('button[type="submit"]').click()

    // assert: parameter searched
    cy.url().should('contains', 'nasa', {matchCase:false})

    // click on first result
    cy.get('[class="entry-title"]').first().click()

    // assert article searched
    cy.url().should('contains', 'nasa')

  })
})

// cy.contains('type').click()

// cy.url().should('include', '/commands/actions')

// cy.get('#email1').type('fake@email.com')

// cy.get('#email1').should('have.value', 'fake@email.com')

// cy.contains('.type()').click()

// cy.url().should('include', 'docs.cypress.io/api/commands/type')